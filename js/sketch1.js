var w=8, d=10, n=50;
var cnum, col;

function rand(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function setup() {
  cnum=n*n;
  col=Array();
	createCanvas(800, 500);
	stroke(0);
	frameRate(1);
}

function draw() {
	for (var i=0; i<cnum; i++) {
    col[i]=color(random(255), random(255), random(255));
  }
  for (var x=1; x<=n; x++) {
    for (var y=1; y<=n; y++) {
      fill(col[x*y]);
      rect(x*d, y*d, w, w);
    }
  }
}
