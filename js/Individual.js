function Individual(x, y, dx, dy, nf, mf, th) {
  this.pos=[x, y]; // position
  this.dims=[dx, dy]; // dimensions of the grid
  this.nf=nf; // number of features
  this.mf=mf; // max value of each feature
  this.state=[];
  this.hopTransform=[[0,1],[1,0],[0,-1],[-1,0]];
  this.actneighbor=[]
  this.threshold=th;
  this.color={};

  this.populateAtRandom = function() {
    for (var n=0; n<this.nf; n++) {
      append(this.state, floor(random(this.mf)));
    }
  };

  this.fetchActNeighbor = function() {
    var direction=floor(random(0, 4));
    var transform=this.hopTransform[direction];
    var newneighbor;
    newneighbor=this.pos.slice();
    newneighbor[0]+=transform[0];
    newneighbor[1]+=transform[1];
    this.actneighbor=this.autoWrap(newneighbor);
    return this.actneighbor;
  };

  this.setColor=function(c) {
    this.color=c;
  };

  this.getColor=function() {
    return this.color;
  }

  this.getCoords = function() {
    return this.pos;
  };

  this.getFeatures = function() {
    return this.state;
  };

  this.getFeatureString = function() {
    return this.state.join("");
  };

  this.getFeature=function(n) {
    return this.state[n];
  };

  this.getSimilarity=function(v1, v2) {
    var sum=0.0;
    var smax=this.nf*this.mf;
    for (var i=0; i<v1.length; i++) {
      sum+=abs(v2[i]-v1[i]);
    }
    return 1-(sum/smax);
  };

  this.copyFromNeighborStates = function(nstates, n) {
    var s=this.getSimilarity(this.state, nstates);
    if (s>=this.threshold) {
      this.state[n]=nstates[n];
    }
    //this.state[n]=nstates[n];
  };

  this.autoWrap = function(pos) {
    if (pos[0]<0) {
      pos[0]+=this.dims[0];
    } else if (pos[0]>=this.dims[0]) {
      pos[0]-=(this.dims[0]-1);
    }
    if (pos[1]<0) {
      pos[1]+=this.dims[1];
    } else if (pos[1]>=this.dims[0]) {
      pos[1]-=(this.dims[1]-1);
    }
    return pos;
  };

  this.populateAtRandom();
}
