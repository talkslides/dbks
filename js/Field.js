function Field(dx, dy, nf, mf, th) {
  this.dims = [dx, dy];
  this.data = [];
  this.stepdata = [];
  this.nf = nf;
  this.mf = mf;
  this.threshold = th;
  this.colvec=[]

  this.doInit = function() {
    this.data = Array(this.dims[0]);
    for (var x = 0; x < this.dims[0]; x++) {
      var row = Array(this.dims[1]);
      for (var y = 0; y < this.dims[1]; y++) {
        row[y] = new Individual(x, y, this.dims[0], this.dims[1], this.nf, this.mf, this.threshold);
      }
      this.data[x] = row;
    }
  };

  this.oneStep = function() {
    this.stepdata = [];
    this.stepdata['featurenum'] = this.getRandomFeatureNum();
    var rx = floor(random(0, this.dims[0]));
    var ry = floor(random(0, this.dims[0]));
    this.stepdata['individual']=this.data[rx][ry];
    var ind=this.stepdata['individual'];
    var ncoords=ind.fetchActNeighbor();
    this.stepdata['neighbor'] = this.data[ncoords[0]][ncoords[1]];
    this.stepdata['individual_feature'] = ind.getFeature(this.stepdata['featurenum']);
    this.stepdata['neighbor_feature'] = this.stepdata['neighbor'].getFeature(this.stepdata['featurenum']);
    this.stepdata['individual'].copyFromNeighborStates(this.stepdata['neighbor'].getFeatures(), this.stepdata['featurenum']);
    //this.dumpStep();
  };

  this.dumpData=function() {
    for (var x = 0; x < this.dims[0]; x++) {
      for (var y = 0; y < this.dims[1]; y++) {
        print(x,y, this.data[x][y].getFeatures());
      }
    }
  };

  this.calcColorVector=function() {
    this.colvec=[];
    var key, col, count=0;
    for (var x = 0; x < this.dims[0]; x++) {
      for (var y = 0; y < this.dims[1]; y++) {
        col=color(random(255), random(255), random(255));
        key=this.data[x][y].getFeatureString();
        this.colvec[key]=col;
        count++;
      }
    }
  };

  this.decorateIndividuals=function() {
    for (var x = 0; x < this.dims[0]; x++) {
      for (var y = 0; y < this.dims[1]; y++) {
        this.data[x][y].setColor(this.colvec[this.data[x][y].getFeatureString()]);
      }
    }
  };

  this.getColor=function(x, y) {
    return this.data[x][y].getColor();
  };

  this.getRandomFeatureNum = function() {
    var rn=random(0, this.nf);
    rn=floor(rn);
    return rn;
  };

  this.getRandomIndividual = function() {
    var rx = random(0, this.dims[0]);
    var ry = random(0, this.dims[0]);
    return this.data[rx, ry];
  };

  this.dumpStep = function() {
    var ic = this.stepdata['individual'].getCoords();
    console.log("Individual coords: " + ic.join(','));
    var nc = this.stepdata['neighbor'].getCoords();
    console.log("Neighbor coords: " + nc.join(','));
    console.log("Feature num: " + this.stepdata['featurenum']);
    console.log("Individual feature: " + this.stepdata['individual_feature']);
    console.log("Neighbor feature: " + this.stepdata['neighbor_feature']);
    console.log("IStates " + this.stepdata['individual'].getFeatures().join(','));
    console.log("NStates " + this.stepdata['neighbor'].getFeatures().join(','));
  };

  this.doInit()

}
