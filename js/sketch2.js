var w=15, d=17;
var dx=30, dy=30;
var numfeatures=3, mf=6, threshold=0.1;
var nsims=100000;

function setup() {
	createCanvas(800, 520);
	stroke(0);
	var field=new Field(dx, dy, numfeatures, mf, threshold);
  for (var step=0; step<nsims; step++) {
    field.oneStep();
  }
  field.calcColorVector();
  field.decorateIndividuals();
  var coords=[];
  for (var x=0; x<dx; x++) {
    for (var y=0; y<dy; y++) {
      fill(field.getColor(x, y));
      rect(x*d, y*d, w, w);
    }
  }
}

function draw() {
}
